
const FIRST_NAME = "Alexandra";
const LAST_NAME = "Smeada";
const GRUPA = "1083";

/**
 * Make the implementation here
 */
function numberParser(input) {
  
    if(input< Number.MIN_SAFE_INTEGER  || input > Number.MAX_SAFE_INTEGER)
     return NaN;
     
    return parseInt(input);
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

